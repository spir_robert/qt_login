/********************************************************************************
** Form generated from reading UI file 'loginwin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWIN_H
#define UI_LOGINWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_loginWin
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;

    void setupUi(QWidget *loginWin)
    {
        if (loginWin->objectName().isEmpty())
            loginWin->setObjectName(QStringLiteral("loginWin"));
        loginWin->resize(223, 116);
        label = new QLabel(loginWin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 47, 13));
        label_2 = new QLabel(loginWin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 40, 47, 13));
        lineEdit = new QLineEdit(loginWin);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(80, 10, 113, 20));
        lineEdit_2 = new QLineEdit(loginWin);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(80, 40, 113, 20));
        lineEdit_2->setEchoMode(QLineEdit::Password);
        pushButton = new QPushButton(loginWin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(70, 70, 75, 23));

        retranslateUi(loginWin);
        QObject::connect(pushButton, SIGNAL(clicked()), loginWin, SLOT(ok_clic()));

        QMetaObject::connectSlotsByName(loginWin);
    } // setupUi

    void retranslateUi(QWidget *loginWin)
    {
        loginWin->setWindowTitle(QApplication::translate("loginWin", "loginWin", 0));
        label->setText(QApplication::translate("loginWin", "Meno", 0));
        label_2->setText(QApplication::translate("loginWin", "Heslo", 0));
        pushButton->setText(QApplication::translate("loginWin", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class loginWin: public Ui_loginWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWIN_H
