/********************************************************************************
** Form generated from reading UI file 'qt_login.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_LOGIN_H
#define UI_QT_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_qt_loginClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *qt_loginClass)
    {
        if (qt_loginClass->objectName().isEmpty())
            qt_loginClass->setObjectName(QStringLiteral("qt_loginClass"));
        qt_loginClass->resize(342, 253);
        centralWidget = new QWidget(qt_loginClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(90, 70, 141, 61));
        qt_loginClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(qt_loginClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 342, 21));
        qt_loginClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(qt_loginClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        qt_loginClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(qt_loginClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        qt_loginClass->setStatusBar(statusBar);

        retranslateUi(qt_loginClass);
        QObject::connect(pushButton, SIGNAL(clicked()), qt_loginClass, SLOT(gombik_clic()));

        QMetaObject::connectSlotsByName(qt_loginClass);
    } // setupUi

    void retranslateUi(QMainWindow *qt_loginClass)
    {
        qt_loginClass->setWindowTitle(QApplication::translate("qt_loginClass", "qt_login", 0));
        pushButton->setText(QApplication::translate("qt_loginClass", "Prihl\303\241senie", 0));
    } // retranslateUi

};

namespace Ui {
    class qt_loginClass: public Ui_qt_loginClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_LOGIN_H
