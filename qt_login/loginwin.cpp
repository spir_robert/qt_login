﻿#include "loginwin.h"

loginWin::loginWin(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
	//odstranime z nasho okna krizik na zatvorenie vpravo hore, chceme aby jedina moznost
	//na zavretie bola cez stlacenie ok
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
}

loginWin::~loginWin() {
	
}

QString loginWin::DajMeno()
{
	return meno;
}

QString loginWin::DajHeslo()
{
	return heslo;
}

void loginWin::ok_clic()
{
	meno = ui.lineEdit->text();
	heslo = ui.lineEdit_2->text();
	close();
}
