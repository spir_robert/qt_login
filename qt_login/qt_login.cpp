#include "qt_login.h"

qt_login::qt_login(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	//do zoznamov mien a hesiel si pridam nejake polozky
	mena.push_back("janko");
	mena.push_back("ferko");

	hesla.push_back("heslo1");
	hesla.push_back("heslo2");
}

qt_login::~qt_login()
{

}

void qt_login::gombik_clic()
{
	//vytvorim si objekt mojho login window
	loginWin login;
	QMessageBox mbox;
	//zobrazim okno
	login.exec();
	bool ok;
	//zistim si, ake tam bolo zadane meno a zistim jeho index v mojom poli mien
	//ak sa tam to meno nenachadza, tak index bude -1
	int index = mena.indexOf(login.DajMeno());
	//v podmienke sa najprv otestuje index, ak bude -1, tak dalsia cast sa uz nevyhodnocuje
	//v druhej casti kontrolujem, ci zadane heslo sedi s heslom v zozname
	if (index == -1 || login.DajHeslo() != hesla[index])
		ok = false;
	else ok = true;

	//podla vysledku vypisem, co sa stalo a program moze nejak pokracovat dalej....
	if (ok == true)
	{
		mbox.setText("prihlasenie uspesne!");
		mbox.exec();
	}
	else
	{
		mbox.setText("Zle meno alebo heslo pouzivatela!");
		mbox.exec();
	}
}