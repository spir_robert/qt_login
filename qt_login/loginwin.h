﻿#pragma once
#ifndef LOGINWIN_HPP
#define LOGINWIN_HPP
#include <QDialog>
#include "ui_loginwin.h"

class loginWin : public QDialog {
	Q_OBJECT

public:
	loginWin(QWidget * parent = Q_NULLPTR);
	~loginWin();

//dve verejne funkcie, ktorymi viem zistit zadane meno a heslo
	QString DajMeno();
	QString DajHeslo();

public slots:
	void ok_clic();

private:
	Ui::loginWin ui;
	QString meno;
	QString heslo;
};

#endif // LOGINWIN_HPP