#ifndef QT_LOGIN_H
#define QT_LOGIN_H

#include <QtWidgets/QMainWindow>
#include "ui_qt_login.h"
#include <loginwin.h>
#include <QMessageBox>

class qt_login : public QMainWindow
{
	Q_OBJECT

public:
	qt_login(QWidget *parent = 0);
	~qt_login();

public slots:
	void gombik_clic();

private:
	Ui::qt_loginClass ui;

	//zoznamy mien a hesiel
	//namiesto vectora pouzivam QList, funguje rovnako, ale ma daleko viac uzitocnych funkcii
	QList<QString> mena;
	QList<QString> hesla;
};

#endif // QT_LOGIN_H
